#!/bin/sh

if [ -f /tmp/normal_boot ]; then 

	pidofflash=`ps | grep -i -E 'flashled' | grep -v grep`;
	kill -9 $pidofflash;

	gateway_ip=`uci get fm.node.gateway_ip`;

	ping -c 1 $gateway_ip
	rc=$?

	if [[ $rc -eq 0 ]] ; then
		#connected
		#Turn on wifi LEDs
		echo 1 > /sys/class/leds/mt76-phy0/brightness;
		echo 1 > /sys/class/leds/zbt-we826\:green\:wifi/brightness;

		backbone_key=`uci get wireless.mesh_five.key`;
		ap_info=`wget -O - http://$gateway_ip/cgi-bin/handler.cgi?p=$backbone_key`;
		#FreeMesh2GHz || 9876543210 || FreeMesh || 9876543210
		ap_two_ssid=`echo $ap_info | cut -d',' -f1`;
		ap_two_key=`echo $ap_info | cut -d',' -f2`;
		ap_five_ssid=`echo $ap_info | cut -d',' -f3`;
		ap_five_key=`echo $ap_info | cut -d',' -f4`;

		if [ `uci get wireless.ap_two.ssid` != $ap_two_ssid ] || [ `uci get wireless.ap_two.key` != $ap_two_key  ] || [ `uci get wireless.ap_five.ssid` != $ap_five_ssid  ] || [ `uci get wireless.ap_five.key` != $ap_five_key  ]; then
			uci set wireless.ap_two.ssid=$ap_two_ssid;
			uci set wireless.ap_two.key=$ap_two_key;
			uci set wireless.ap_five.ssid=$ap_five_ssid;
			uci set wireless.ap_five.key=$ap_five_key;
			uci commit wireless;
			wifi;
		fi
	else
		#not connected
		#Alternate LEDs
		/freemesh/flashled.sh 1 &
	fi
fi
#else - do nothing since we are controlling the leds elsewhere
